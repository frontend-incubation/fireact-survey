import logo from './logo.svg';
import './App.css';
import FireactSurvey from './FireactSurvey';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <FireactSurvey />
    </div>
  );
}

export default App;
