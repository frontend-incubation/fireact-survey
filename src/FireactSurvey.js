import React, { Component } from 'react';
import firebase from 'firebase';
const uuid = require('uuid');

var firebaseConfig = {
  apiKey: "AIzaSyBgCQsNy0dKsC0ZvJYbQwAOslXhFRYg62o",
  authDomain: "fireact-survey.firebaseapp.com",
  databaseURL: "https://fireact-survey-default-rtdb.firebaseio.com",
  projectId: "fireact-survey",
  storageBucket: "fireact-survey.appspot.com",
  messagingSenderId: "980392760798",
  appId: "1:980392760798:web:4c712ec7e9f92106ea394f",
  measurementId: "G-WM6ET1NLYK"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

class FireactSurvey extends Component {
  constructor(props) {
    super(props)

    this.state = {
      uid: uuid.v1(),
      studentName: '',
      answers: {
        answers1: '',
        answers2: '',
        answers3: '',
      },
      isSubmitted: false,
    };
  }
  render() {
    let studentName;
    let questions;

    if (this.state.studentName === '' && this.state.isSubmitted === false) {
      studentName = <div>
        <h1>Hey Student, please let us know your name: </h1>
        <form>
          <input className="namy" type="text" placeholder="Enter your name" ref="name" />
        </form>
      </div>
    }

    return (
      <div>
        {studentName}
        --------------------------------------
        {questions}
      </div>
    );
  }
}

export default FireactSurvey;
